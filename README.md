# Environment Setup Configuration Repository
## What even is this?
This is a repository for storing my configurations for various programs so that they are consitent across my workspaces. It also speeds up configuration!

## Structure

1. Top Level
    1. setup.sh -- setup script to populate things like zshrc, vimrc, emacs.d, bashrc, ect.
    2. bashrc, zshrc, gitconfig, .spacemacs, ect
2. .vim
    1. vimrc
    2. submodules
3. ghc -- ghci (haskell interpreter) configs
4. tools -- setup scripts for various tools
    1. emacs -- specialized emacs .el configurations
    2. cargo_config -- rust build system configurations
    3. omzsh_install.sh -- installation script for omzsh (better features for zsh + themes)
    4. pyenv_install.sh -- installation for pyenv
    5. rust_install.sh -- installs rust tool chain
    6. rustenv.sh -- put rust tool chains into PATH
    7. pyenvenv.sh -- put pyenv into PATH
;; profiles
(("default" . ((user-emacs-directory . "~/environment-setup/doom-emacs")))
 ("doom" . ((user-emacs-directory . "~/environment-setup/doom-emacs")))
 ("spacemacs" . ((user-emacs-directory . "~/environment-setup/spacemacs"))))

#!/usr/bin/env bash
VERSION=$(curl https://download.eclipse.org/jdtls/snapshots/latest.txt)
FILE=/tmp/$VERSION
EDIR=~/.emacs.d/.cache/lsp/eclipse.jdt.ls
#download
curl https://download.eclipse.org/jdtls/snapshots/$VERSION -o $FILE
tar -xf $FILE -C $EDIR

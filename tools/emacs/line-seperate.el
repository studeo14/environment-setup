;; line-seperate.el

(defun line-seperate (split-word indentation)
  "Seperate a line based on the given input"
  (interactive (list (let ((default-split-word (if-let ((temp-def (thing-at-point 'word 'no-properties)))
                                                   ;; if thing at point
                                                   temp-def
                                                 ;; else
                                                 (progn
                                                   (forward-whitespace 1)
                                                   (thing-at-point 'word 'no-properties)))))
                       (read-string "Enter word to split on: " default-split-word))
                     (current-indentation)))
  ;; if region is valid
  (progn
    (save-excursion
      (if (use-region-p)
          (progn
            ;; cut
            (if (<= (point) (mark))
                (kill-region (point) (mark))
              (kill-region (mark) (point)))
            (let ((text (car kill-ring-yank-pointer)))
              (progn
                (set-mark (point))
                (insert (replace-regexp-in-string split-word
                                                  (concat "\n" split-word)
                                                  (replace-regexp-in-string "\n" "" text)))
                (newline)
                (indent-rigidly (mark)
                                (point)
                                indentation))))
        ;; else
        (progn
          ;; set point and mark to encapsulate current line
          (end-of-line)
          (set-mark (point))
          (beginning-of-line)
          (kill-region (point) (mark))
          ;; replace split word
          (let ((text (car kill-ring-yank-pointer)))
            (progn
              (set-mark (point))
              (insert (replace-regexp-in-string split-word
                                                (concat "\n" split-word)
                                                (replace-regexp-in-string "\n" "" text)))
              (newline)
              (indent-rigidly (mark)
                              (point)
                              indentation)))))))
  (delete-trailing-whitespace))

(provide 'line-seperate)

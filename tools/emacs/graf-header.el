(use-package header2
  :defer 10
  :custom
  (graf-header/comment-start comment-start)
  (graf-header/comment-delim header-prefix-string)
  (graf-header/comment-end   comment-end)
  :config
  ;; header parts defines
  ;; define if wanted
  (defsubst graf-header/pre-header ()
    "Pre-header information."
    (insert ""))

  (defsubst graf-header/post-header ()
    "Post-header information."
    (insert ""))

  (defsubst graf-header/start-header ()
    "Start of the header"
    (insert graf-header/comment-start "\n"))

  (defsubst graf-header/main-header ()
    "Insert the main text of the Graf header"
    (insert graf-header/comment-delim "Graf Research Corporation\n")
    (insert graf-header/comment-delim "Copyright (c) "
            (shell-command-to-string "echo -n $(date +%Y-%Y)") "\n")
    (insert graf-header/comment-delim "Graf Research Proprietary - Do Not Distribute\n"))

  (defsubst graf-header/end-header ()
    "End of the header"
    (insert graf-header/comment-end "\n"))

  (defsubst graf-header/make-header ()
    "Make header function."
    (interactive)
    (graf-header/start-header)
    (graf-header/pre-header)
    (graf-header/main-header)
    (graf-header/post-header)
    (graf-header/end-header))

  ;; mode hooks
  (defconst graf-header/c-style-hooks '(verilog-mode-hook
                                        c-mode-common-hook
                                        java-mode-hook
                                        javascript-mode-hook)
    "List of hooks to use c-style headers.")
  (defconst graf-header/sh-style-hooks '(python-mode-hook
                                         sh-mode-hook)
    "List of hooks to use sh-style headers.")
  (defconst graf-header/xml-style-hooks '(xml-mode-hook
                                          html-mode-hook
                                          vue-mode-hook)
    "List of hooks to use xml-style headers.")

  (dolist (hook graf-header/c-style-hooks)
    (add-hook hook
              (lambda ()
                (setq graf-header/comment-start "/*")
                (setq graf-header/comment-delim " * ")
                (setq graf-header/comment-end   " */"))))
  (dolist (hook graf-header/sh-style-hooks)
    (add-hook hook
              (lambda ()
                (setq graf-header/comment-start "#")
                (setq graf-header/comment-delim "# ")
                (setq graf-header/comment-end   "#"))))
  (dolist (hook graf-header/xml-style-hooks)
    (add-hook hook
              (lambda ()
                (setq graf-header/comment-start "<!--/*")
                (setq graf-header/comment-delim " * "    )
                (setq graf-header/comment-end   "*/-->" ))))

  ;; connect to header2
  (setq make-header-hook '(graf-header/make-header)))

;; provide
(provide 'graf-header)

#!/usr/bin/env bash
git clone https://gitlab.matrix.org/matrix-org/olm.git
cd olm
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
sudo make install
cd ../..
dest=~/environment-setup/tools/weechat-matrix
git clone https://github.com/poljar/weechat-matrix.git $dest
cd $dest
pip install -r requirements.txt
make install

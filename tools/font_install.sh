#!/usr/bin/env bash
FONT_DIR=$(mktemp -d)
git clone \
    --depth 1 \
    --filter=blob:none \
    --no-checkout \
    https://github.com/ryanoasis/nerd-fonts $FONT_DIR \
    ;
{
    cd $FONT_DIR
    git checkout master -- install.sh patched-fonts/CascadiaCode
    bash install.sh
}
rm -rf $FONT_DIR

#!/usr/bin/env bash

TMP=`mktemp`
google_dns=("8.8.8.8" "8.8.4.4")
trap ctrlC INT

removeTempFiles()
{
	rm -f $TMP
}

ctrlC()
{
	echo
	echo "Trapped Ctrl-C, removing temporary files"
	removeTempFiles
	stty sane
}

getCandidates()
{
    /mnt/c/Windows/System32/WindowsPowerShell/v1.0/powershell.exe -Command "Get-DnsClientServerAddress -AddressFamily ipv4 | Select-Object -ExpandProperty ServerAddresses" | sort | uniq
}

getMyRoute()
{
    ip route get 1.2.3.4 | awk '{print $3}'
}

echo "Current resolv.conf"
echo "-------------------"
cat /etc/resolv.conf

echo
echo "Creating new resolv.conf"
echo "------------------------"

base_candidates=$(getCandidates)
my_route=$(getMyRoute)
base_candidates=("$my_route" "${google_dns[@]}" "${base_candidates[@]}")
{
    head -1 /etc/resolv.conf | grep '^#.*generated'
    tail -n+2 /etc/resolv.conf | grep -v '^nameserver'
    for i in ${base_candidates[@]}; do
        echo nameserver $i
    done
} | tr -d '\r' | tee $TMP

(set -x; sudo cp -i $TMP /etc/resolv.conf)

removeTempFiles



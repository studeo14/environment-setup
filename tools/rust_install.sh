curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs -sSf | sh
mkdir -p ~/.cargo
ln -sf ~/environment/tools/cargo_config ~/.cargo/config

;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Steven Frederiksen"
      user-mail-address "steven@grafresearch.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-font
      (font-spec :family "CaskaydiaCove Nerd Font Mono" :size 10))
(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")
(setq rmh-elfeed-org-files '("~/org/elfeed.org"))
(setq-default browse-url-generic-program (executable-find "firefox")
              browse-url-browser-function 'browse-url-generic)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)
(setq-default evil-escape-key-sequence "wj")

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
(setq verilog-auto-lineup                   'all
        verilog-indent-level                    4
        verilog-indent-level-module             4
        verilog-indent-level-declaration        4
        verilog-indent-declaration-macros       nil
        verilog-indent-lists                    t
        verilog-indent-level-behavioral         4
        verilog-indent-level-directive          0
        verilog-cexp-indent                     4
        verilog-case-indent                     4
        verilog-auto-newline                    t
        verilog-auto-indent-on-newline          t
        verilog-tab-always-indent               nil
        verilog-tab-to-comment                  nil
        verilog-indent-begin-after-if           t
        verilog-align-ifelse                    t
        verilog-minimum-comment-distance        40
        verilog-highlight-p1800-keywords        t
        verilog-highlight-grouping-keywords     nil
        verilog-highlight-modules               nil
        verilog-highlight-includes              t
        verilog-auto-declare-nettype            nil
        verilog-auto-wire-type                  nil
        verilog-auto-endcomments                t
        verilog-auto-delete-trailing-whitespace t
        verilog-auto-ignore-concat              nil
        verilog-auto-read-includes              nil
        verilog-auto-save-policy                nil
        verilog-auto-update-tick                nil
        verilog-auto-last-file-locals           nil
)

(add-hook! 'elfeed-search-mode-hook 'elfeed-update)

(after! 'projectile
        (setq 'projectile-indexing-method 'alien)
        (setq 'projectile-enable-caching t)
  )

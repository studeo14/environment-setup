if command -v lsd &> /dev/null
then
    alias ls="lsd"
    alias lsl="ls -l"
    alias lsa="ls -a"
    alias lsla="ls -al"
else
    alias ls="ls --color=auto"
    alias lsl="ls -l --color=auto"
    alias lsa="ls -a --color=auto"
    alias lsla="ls -al --color=auto"
fi
alias cls="clear"
alias tma="tmux attach"
alias vi="vim"
alias py="python3"
alias nios2_shell="/opt/altera/14.1/nios2eds/nios2_command_shell.sh"
alias init_opencl="source ~/intelFPGA/16.1/hld/init_opencl.sh"
alias ec="emacsclient -c -a emacs"
alias drop_cache="sudo sh -c \"echo 3 >'/proc/sys/vm/drop_caches' && swapoff -a && swapon -a && printf '\n%s\n' 'Ram-cache and Swap Cleared'\""
atree()
{
    find . -print 2>/dev/null|awk '!/\.$/ {for (i=1;i<NF;i++){d=length($i);if ( d < 5  && i != 1  )d=5;printf("%"d"s","|")}print "---"$NF}'  FS='/'
}

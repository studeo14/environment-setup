# bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

export PS1="\[\e]0;\w\a\]\n\[\e[32m\]\u\[\e[33m\]@\[\e[32m\]\\h: \[\e[33m\]\w\[\e[0m\]\\n$ "

# aliases
if [ -f ~/environment-setup/bash_aliases ]; then
	. ~/environment-setup/bash_aliases
fi
if [ -n "$SSH_CLIENT"  ] || [ -n "$SSH_TTY"  ]; then
    SESSION_TYPE="remote"
    # many other tests omitted
else
    case $(ps -o comm= -p $PPID) in
        sshd|*/sshd) SESSION_TYPE="remote";;
    esac
fi
if [[ "$SESSION_TYPE" -ne "remote" ]]; then
    export DISPLAY=$(ip route get 1.2.3.4 | awk '{print $3}'):0.0
    # export DISPLAY=$(hostname -I | awk '{print $1}')
fi
if [[ -d "$HOME/.local/bin" ]]; then
    export PATH=$HOME/.local/bin:$PATH
fi
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

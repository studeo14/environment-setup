#!/usr/bin/env bash
echo "Setting up cscope"
mkdir -p ~/environment-setup/.vim/plugin
wget -O ~/environment-setup/.vim/plugin/cscope.vim http://cscope.sourceforge.net/cscope_maps.vim
echo "Done..."
echo "Setting up submodules"
git submodule init
git submodule update
echo "Done..."
echo "Setting up JS-XML plugin..."
cp ~/environment-setup/.vim/bundle/xml-vim/ftplugin/html.vim ~/environment-setup/.vim/bundle/xml-vim/ftplugin/javascript.vim
cp ~/environment-setup/.vim/bundle/xml-vim/ftplugin/html.vim ~/environment-setup/.vim/bundle/xml-vim/ftplugin/js.vim
echo "Done..."
echo "Setting up environment"
echo "source ~/environment-setup/bashrc" > ~/.bash_profile
echo "source ~/.bash_profile" > ~/.bashrc
ln -sf ~/environment-setup/.vim ~/.vim
echo "Enter in your name for Git: "
read name
echo "Enter in your email for Git: "
read email
cp ~/environment-setup/gitconfig ~/environment-setup/.gitconfig
ln -sf ~/environment-setup/.gitconfig ~/.gitconfig
sed -i "s/\(name = \).*/\1$name/g; s/\(email = \).*/\1$email/g" ~/environment-setup/.gitconfig
echo "Setting up spacemacs"
ln -sf ~/environment-setup/chemacs2 ~/.emacs.d
ln -sf ~/environment-setup/.spacemacs ~/.spacemacs
ln -sf ~/environment-setup/emacs-profiles.el ~/.emacs-profiles.el
ln -sf ~/environment-setup/doom.d ~/.doom.d
echo "Setting up mercurial"
ln -sf ~/environment-setup/hgrc ~/.hgrc
ln -sf ~/environment-setup/.zshrc ~/.zshrc
ln -sf ~/environment-setup/p10k.zsh ~/.p10k.zsh
ln -sf ~/environment-setup/ghc ~/.ghc
echo "Done..."
